/* *****************************************************************
 *
 * Download latest Blinker library here:
 * https://github.com/blinker-iot/blinker-library/archive/master.zip
 * 
 * 
 * Blinker is a cross-hardware, cross-platform solution for the IoT. 
 * It provides APP, device and server support, 
 * and uses public cloud services for data transmission and storage.
 * It can be used in smart home, data monitoring and other fields 
 * to help users build Internet of Things projects better and faster.
 * 
 * Make sure installed 2.7.4 or later ESP8266/Arduino package,
 * if use ESP8266 with Blinker.
 * https://github.com/esp8266/Arduino/releases
 * 
 * Make sure installed 1.0.4 or later ESP32/Arduino package,
 * if use ESP32 with Blinker.
 * https://github.com/espressif/arduino-esp32/releases
 * 
 * Docs: https://diandeng.tech/doc
 *       
 * 
 * *****************************************************************
 * 
 * Blinker 库下载地址:
 * https://github.com/blinker-iot/blinker-library/archive/master.zip
 * 
 * Blinker 是一套跨硬件、跨平台的物联网解决方案，提供APP端、设备端、
 * 服务器端支持，使用公有云服务进行数据传输存储。可用于智能家居、
 * 数据监测等领域，可以帮助用户更好更快地搭建物联网项目。
 * 
 * 如果使用 ESP8266 接入 Blinker,
 * 请确保安装了 2.7.4 或更新的 ESP8266/Arduino 支持包。
 * https://github.com/esp8266/Arduino/releases
 * 
 * 如果使用 ESP32 接入 Blinker,
 * 请确保安装了 1.0.4 或更新的 ESP32/Arduino 支持包。
 * https://github.com/espressif/arduino-esp32/releases
 * 
 * 文档: https://diandeng.tech/doc
 *       
 * 
 * 侯长军 注意 LED和继电器 HIGH是关闭，LOW是打开
 * 
 * *****************************************************************/

#define BLINKER_WIFI
#define BLINKER_MIOT_LIGHT

#include <Blinker.h>

//菜园水阀的开关，auth是在电灯科技的app里面注册设备的时候生成的secret key（选择阿里云连接）
char auth[] = "f10200c3ec22";
char ssid[] = "hougejinguiyuan";
char pswd[] = "13564385975";

// 新建组件对象
BlinkerButton Button1("btn-abc");
BlinkerNumber Number1("num-abc");

int counter = 0;

//小米电源开关控制回调函数
void miotPowerState(const String & state)
{
    BLINKER_LOG("need set power state: ", state);

    if (state == BLINKER_CMD_ON) {
        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(0, LOW);
        BLINKER_LOG("### set power state to on");
        BlinkerMIOT.powerState("on");
        BlinkerMIOT.print();

        button1_callback(BLINKER_CMD_ON);
    }
    else if (state == BLINKER_CMD_OFF) {
        digitalWrite(LED_BUILTIN, HIGH);
        digitalWrite(0, HIGH);
        BLINKER_LOG("### set power state to off");
        BlinkerMIOT.powerState("off");
        BlinkerMIOT.print();

        button1_callback(BLINKER_CMD_OFF);
    }
}
//小米状态查询回调函数
void miotQuery(int32_t queryCode)
{
    BLINKER_LOG("MIOT Query codes: ", queryCode);

    switch (queryCode)
    {
        case BLINKER_CMD_QUERY_PM25_NUMBER :
            BLINKER_LOG("MIOT Query PM25");
            BlinkerMIOT.pm25(20);
            BlinkerMIOT.print();
            break;
        case BLINKER_CMD_QUERY_HUMI_NUMBER :
            BLINKER_LOG("MIOT Query HUMI");
            BlinkerMIOT.humi(20);
            BlinkerMIOT.print();
            break;
        case BLINKER_CMD_QUERY_TEMP_NUMBER :
            BLINKER_LOG("MIOT Query TEMP");
            BlinkerMIOT.temp(20);
            BlinkerMIOT.print();
            break;
        case BLINKER_CMD_QUERY_TIME_NUMBER :
            BLINKER_LOG("MIOT Query Time");
//            BlinkerMIOT.time(millis()); compile error
            BlinkerMIOT.print();
            break;
        default :
            BlinkerMIOT.temp(20);
            BlinkerMIOT.humi(20);
            BlinkerMIOT.pm25(20);
            BlinkerMIOT.co2(20);
            BlinkerMIOT.print();
            break;
    }
}

// 按下按键即会执行该函数
void button1_callback(const String & state)
{  
    BLINKER_LOG("about to set button state: ", state);

    if (state == BLINKER_CMD_OFF) {
        BLINKER_LOG("Toggle off!");

        digitalWrite(LED_BUILTIN, HIGH);
        digitalWrite(0, HIGH);
        Button1.icon("icon_1");
        Button1.color("#00FF00");
        Button1.text("已停止");
        Button1.textColor("#00FF00");
        Button1.print("off");
    }
    else if (state == BLINKER_CMD_ON) {
        BLINKER_LOG("Toggle on!");

        digitalWrite(LED_BUILTIN, LOW);
        digitalWrite(0, LOW);
        Button1.icon("icon_1");
        Button1.color("#FF0000");
        Button1.textColor("#FF0000");
        Button1.text("正在浇水");
        Button1.print("on");
    }

}

// 如果未绑定的组件被触发，则会执行其中内容
void dataRead(const String & data)
{
    BLINKER_LOG("Blinker readString: ", data);
    counter++;
    Number1.print(counter);
}

void setup()
{
    // 初始化串口
    Serial.begin(115200);
    BLINKER_DEBUG.stream(Serial);
    BLINKER_DEBUG.debugAll();
    
    // 初始化有LED的IO
    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);
    // init relay device
    pinMode(0, OUTPUT);
    digitalWrite(0, HIGH);
    // 初始化blinker
    Blinker.begin(auth, ssid, pswd);
    Blinker.attachData(dataRead);

    Button1.attach(button1_callback);
    BlinkerMIOT.attachPowerState(miotPowerState);
    BlinkerMIOT.attachQuery(miotQuery);

}

void loop() {
    Blinker.run();
}
